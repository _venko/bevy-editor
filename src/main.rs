use bevy::{
  diagnostic::{DiagnosticsStore, FrameTimeDiagnosticsPlugin},
  input::{
    keyboard::KeyboardInput,
    ButtonState,
  },
  prelude::*,
  window::PrimaryWindow,
};

const TITLE: &'static str = "Venko's Sick Fuckin' Text Editor";
const CURSOR: &'static str = "|";
const FONT_SIZE: f32 = 48.0;

fn main() {
  App::new()
    .add_plugins(DefaultPlugins.set(WindowPlugin {
      primary_window: Some(Window {
        title: TITLE.to_string(),
        ..Default::default()
      }),
      ..Default::default()
    }))
    .add_plugins(FrameTimeDiagnosticsPlugin::default())
    .add_event::<EditorEvent>()
    .add_systems(Startup, setup)
    .add_systems(Update, update_window_fps_display)
    .add_systems(Update, (handle_keyboard_input, handle_editor_event).chain())
    .run();
}

#[derive(Component)]
struct EditorText;

#[derive(Default, Component, PartialEq, Eq)]
enum EditorMode {
  #[default]
  Normal,
  Insert,
}

#[derive(Bundle)]
struct EditorState {
  mode: EditorMode,
}

#[derive(Event)]
enum EditorEvent {
  SetNormalMode,
  SetInsertMode,
  Insert(String),
  Backspace,
}

fn setup(mut commands: Commands, asset_server: Res<AssetServer>) {
  commands.spawn(Camera2dBundle::default());

  let font = asset_server.load("fonts/american-typewriter.ttf");
  let text_style = TextStyle {
    font,
    font_size: FONT_SIZE,
    color: Color::WHITE,
  };
  let text = Text::from_sections([
    TextSection::new("", text_style.clone()),
    TextSection::new(CURSOR, text_style.clone()),
  ]);

  commands.spawn((
    TextBundle {
      text,
      ..Default::default()
    }
    .with_style(Style {
      position_type: PositionType::Absolute,
      top: Val::Px(5.0),
      left: Val::Px(5.0),
      ..Default::default()
    }),
    EditorText,
  ));

  commands.spawn(EditorState {
    mode: EditorMode::default(),
  });
}

// fn handle_mouse_input(
//   mut mouse_button_events: EventReader<MouseButtonInput>,
//   mut mouse_motion_events: EventReader<MouseMotion>
// ) {
//   mouse_button_events.read().for_each(|event| info!("{event:?}"));
//   mouse_motion_events.read().for_each(|event| info!("{event:?}"));
// }

// fn log_mouse_position(q_windows: Query<&Window, With<PrimaryWindow>>) {
//   if let Some(position) = q_windows.single().cursor_position() {
//     info!("{position:?}");
//   }
// }

fn handle_keyboard_input(
  mut keyboard_input_events: EventReader<KeyboardInput>,
  mut editor_event_writer: EventWriter<EditorEvent>,
  mode_query: Query<&EditorMode>,
) {
  let mode = mode_query.single();
  for event in keyboard_input_events.read() {
    if event.state != ButtonState::Pressed {
      continue;
    }
    match *mode {
      EditorMode::Normal => match event.key_code {
        KeyCode::KeyI => _ = editor_event_writer.send(EditorEvent::SetInsertMode),
        _ => info!("Normal mode: {event:?}"),
      },
      EditorMode::Insert => match event.key_code {
        KeyCode::Escape => _ = editor_event_writer.send(EditorEvent::SetNormalMode),
        KeyCode::Space => _ = editor_event_writer.send(EditorEvent::Insert(" ".to_string())),
        KeyCode::Enter => _ = editor_event_writer.send(EditorEvent::Insert("\n".to_string())),
        KeyCode::Backspace => _ = editor_event_writer.send(EditorEvent::Backspace),
        _ => match &event.logical_key {
          bevy::input::keyboard::Key::Character(ch) => {
            editor_event_writer.send(EditorEvent::Insert(ch.to_string()));
          }
          _ => info!("Insert mode: {event:?}"),
        },
      },
    }
  }
}

fn handle_editor_event(
  mut editor_event_reader: EventReader<EditorEvent>,
  mut mode_query: Query<&mut EditorMode>,
  mut text_query: Query<&mut Text, With<EditorText>>,
) {
  let mut mode = mode_query.single_mut();
  for event in editor_event_reader.read() {
    match event {
      EditorEvent::SetNormalMode => *mode = EditorMode::Normal,
      EditorEvent::SetInsertMode => *mode = EditorMode::Insert,
      EditorEvent::Insert(ch) => {
        for mut text in &mut text_query {
          let old = &mut text.sections[0].value;
          old.push_str(&ch);
        }
      }
      EditorEvent::Backspace => {
        for mut text in &mut text_query {
          let _ = &mut text.sections[0].value.pop();
        }
      }
    }
  }
}

fn update_window_fps_display(
  diagnostics: Res<DiagnosticsStore>,
  mut window: Query<&mut Window, With<PrimaryWindow>>,
  mode: Query<&EditorMode>,
) {
  if let Some(fps) = diagnostics.get(&FrameTimeDiagnosticsPlugin::FPS) {
    if let Some(avg) = fps.average() {
      let mode = mode.single();
      let mode_str = match mode {
        EditorMode::Normal => "Normal",
        EditorMode::Insert => "Insert",
      };
      let title = format!("{TITLE}: ({mode_str} mode) ({avg:.1} fps)");
      window.single_mut().title = title;
    }
  }
}
